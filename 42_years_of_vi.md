# **42+ years of vi**

---
# **History of vi**

- Developed by Bill Joy and Chuck Haley in 1976 at UCB
- from qed, ed and em (editor for mortals)
- ex open mode evolved into vi
- vi survived the copyright wars and many clones were created
- Modal editor  
  command mode (normal/beep/disappearing character mode)  
  insert mode  
  replace mode  
  ex mode (Q command)  
  open mode only in traditional vi (o command in ex mode)  
  visual mode (select text) only in vim

[//]: # (- "open" is the only ex command missing from vim)

---
# **Early Open Source**

- ed was written in 1969 in assembly on a PDP-7 at Bell Labs
- ed was rewritten in C for PDP-11 in 1973 at Bell Labs
- vi was written in C over a 300 baud modem on PDP11 at UCB
- First released as "ex" in the first Berkeley Software Distribution in 1978
- vi used the ADM3a terminal with cursor control
- BSD 1 was released on 9 track 800cpi tape (22.5MB capacity)
- Licence Encumbered BSD release contained AT&T Unix code until 1991 (Net/2)
- Caldera removed ATT licence restrictions in 2002, freeing up traditional vi
[//]: # (with 12 in 24x80 upper/lowercase, 75-19200 baud, cursor control)

---
# **Ken Thompson and Dennis Ritchie**

![Ken Thompson (sitting) and Dennis Ritchie at PDP-11/20](images/ken_thompson_and_dennis_ritchie_at_pdp11_half.jpg)

---
# **The vi family tree before 1990**

- 1968 ed (Ken Thompson/Dennis Ritchie < qed) (ATT Bell Labs)
- 1976 em "Editor for Mortals" (George Coulouris < ed) (Queen Mary College DECtape)
- 1977 ex/vi 1.1 (Bill Joy/Chuck Haley < ed/em) (AT&T Unix and BSD) "BSD/CDDL"
- 1979 ex/vi 2.0 (Bill Joy) (AT&T Unix and BSD) "BSD/CDDL"
- 1987 stevie Atari ST (Tim Thompson) comp.sys.atari.st "Public Domain"
- 1988 stevie Unix OS/2 Amiga (Tony Andrews) comp.sources.unix "free source"

---
# **Bill Joy**

![Bill Joy](images/that-lack-of-programmability-is-probably-what-ultimately-will-doom-vi-it-cant-extend-its-domain-198364.jpg "That lack of programmability is probably what ultimately will doom vi.  It can't extend its domain.  -- Bill Joy --")

---
# **ADM 3a Keyboard**

<br/>
<br/>
<br/>
<br/>
<br/>

![ADM3a](images/640px-KB_Terminal_ADM3A.svg.png "ADM-3a keyboard")

---
# **ADM 3a**

![ADM3a](images/adm3a.jpg "Lear-Siegler ADM-3a $995")

---
# **Display Editing with Vi**
## Written by Bill Joy and Mark Horton
## annotations by Gunnar Ritter 2002
### The original documentation

[An Introduction to Display Editing with Vi] (http://ex-vi.sourceforge.net/viin/paper.html)

1. Getting started
2. Moving around in the file
3. Making simple changes
4. Moving about; rearranging and duplicating text
5. High level commands
6. Special topics
7. Nitty-gritty details

---
# **The vi family tree after 1990**

- 1990 vile "vi-like emacs" (Paul Fox)  alt.sources "GPL"
- 1990 elvis (Steve Kirkendall) comp.os.minix "Clarified Artistic License"
- 1991 vim (Bram Moolenaar < amiga stevie ) "Vim License"
- 1994 nvi (Keith Bostic < elvis 1.8) BSD 4.4 final CSRG release "BSD License"
- 1995 busybox (Bruce Perens) tiny vi "GPL v2"
- 2002 traditional vi (Gunnar Ritter < 2.11BSD vi) "BSD License"
- 2006 toybox (Rob Landley) fork of busybox "OBSD/Public Domain"
- 2014 neovim (Justin Keyes) fork of vim "Apache 2.0 license/Vim license"
- 2015 sodium (Ticki) redox-os rust vi clone "MIT License"

[//]: # (## Release Program {Author} and licences {past} "Current")
[//]: # (- 1979 Mark Horton assumed lead, added terminfo replacing termcap)


---
# **vi family tree**

![VI](images/42_years_of_vi.png "vi family tree")

---
# **vi**

[Original Vi source 2005-03-25] (http://ex-vi.sourceforge.net/)

    Compared to most of its many clones, the traditional
    vi is a rather small program (the binary size is
    approximately 160 kBytes on i386) just with its
    extremely powerful editing interface, but lacking
    fancy features like multiple undo, multiple screens,
    or syntax highlighting.
    
    This port of vi has generally preserved the original
    style, terminal control, and feature set. It adds
    support for international character sets, including
    multibyte encodings such as UTF-8, and some minor
    enhancements that were not present in BSD vi 3.7,
    but had been included in later vi versions for System
    V or in POSIX.2.

---
# **busybox vi**

## Bruce Perens wrote tiny vi for busybox with GPL2
   Licence controversies in 2006 caused toybox fork  
   GPL lawsuits forced settlements with several companies

## Busybox vi is limited
+ undo
+ colon commands
+ . command
+ set options
+ lots of other limitations

---
# **vim**

## vim is the most comprehensive current implementation

[The Vim book] (http://www.truth.sk/vim/vimbook-OPL.pdf)  
by Steve Oualline published 2001 <br /> 

[Vim in 6k] (https://www.vim.org/6k/features.en.txt)

    Vim ("Vi IMproved") is a "vi clone", ie a program similar to the
    text editor "vi".

    Vim works in textmode in every terminal, but it also has a graphic
    user interface, ie menus and support for the mouse.

    Availability: Vim is available for many platforms
    and has many added features compared to Vi.
    (see http://www.vim.org/viusers.php)
    Vim is compatible to almost all Vi commands -
       except Vi's bugs. ;-)

    Operating Systems: Vim is available for many systems: AmigaOS,
    Atari MiNT, BeOS, DOS, MacOS, NextStep, OS/2, OSF, RiscOS, SGI,
    UNIX, VMS, Win16 + Win32 (Windows95/98/00/NT) - and especially
    FreeBSD and Linux.  :-)

---
# **vim**

    Copyright:
    The copyright is in the hands of the main author
    and maintainer, Bram Moolenaar <bram@vim.org>.
    Vim is "charity-ware", ie you are encouraged
    to make a donation to orphans in Uganda (see
    ":help uganda").

![Bram Moolenaar](images/453px-Bram_Moolenaar_in_2007.jpg "Bram Moolenaar in 2007 at 46")

---
# **gvim**

## mouse friendly version for those who need a gui
![gvim screenshot](images/gvim.png "gvim :ver screenshot")

---
# **relative sizes**
### Gentoo x86-64 with gcc -O2 -mtune=native

   `size /usr/bin/vim /bin/busybox /usr/local/bin/vi`  

   <table border=1>
    <tr>
    <th align=right>text</th>
    <th align=right>data</th>
    <th align=right>bss</th>
    <th align=right>dec</th>
    <th align=right>hex</th>
    <th>filename</th>
    </tr>
    <tr>
    <td align=right>2484432</td>
    <td align=right>163688</td>
    <td align=right>46904</td>
    <td align=right>2695024</td>
    <td align=right>291f70</td>
    <td>/usr/bin/vim</td>
    </tr>
    <tr>
    <td align=right>2228881</td>
    <td align=right>38070</td>
    <td align=right>13562</td>
    <td align=right>2280513</td>
    <td align=right>22cc41</td>
    <td>/bin/busybox</td>
    </tr>
    <tr>
    <td align=right>226768</td>
    <td align=right>5648</td>
    <td align=right>171768</td>
    <td align=right>404184</td>
    <td align=right>62ad8</td>
    <td>/usr/local/bin/vi</td>
    </tr>
   </table>
<br />

### nvi uses it's own libvi

   `size /usr/bin/nvi /usr/lib64/libvi.so`  

   <table border=1>
    <tr>
    <th align=right>text</th>
    <th align=right>data</th>
    <th align=right>bss</th>
    <th align=right>dec</th>
    <th align=right>hex</th>
    <th>filename</th>
    </tr>
    <tr>
    <td align=right>26841</td>
    <td align=right>2064</td>
    <td align=right>224</td>
    <td align=right>29129</td>
    <td align=right>71c9</td>
    <td>/usr/bin/nvi</td>
    </tr>
    <tr>
    <td align=right>438475</td>
    <td align=right>18688</td>
    <td align=right>144</td>
    <td align=right>457307</td>
    <td align=right>6fa5b</td>
    <td>/usr/lib64/libvi.so.0</td>
    </tr>
    <tr>
    <td align=right>465316</td>
    <td align=right>20752</td>
    <td align=right>368</td>
    <td align=right>486436</td>
    <td align=right>76C24</td>
    <td>nvi+libvi</td>
    </tr>
   </table>

---
# **vim commands**
## The following pages show basic vim usage
## Not all commands are included. Look online for more.

+ help command
+ vimtutor
+ .vimrc, .viminfo, .vim
+ vim command grammar
+ vim editing commands
+ vim visual mode commands
+ vim advanced
+ vim plugins
+ what I use

---
# ** help command**
**General help**

    !vim
    :help
    :h
   
**Specific help**

    !vim
    :help tag  

**To Exit**

    !vim
    :q
    :q!

**Vim User Manual Table Of Contents**

    !vim
    :help user-manual
 
---
# ** vimtutor**

## Vim Tutorial

Edit a file with instructions in the file itself

**command line to run tutorial**

    $ vimtutor

**how to run the tutorial from vim help**

    $ vim

    :help tutor

    The approximate time required to complete the tutor is 25-30 minutes,
    depending upon how much time is spent with experimentation.

Especially useful for absolute beginners who fear being bored by learning the basic commands: [Vim Adventure] (http://vim-adventures.com/)  
Starts with teaching you h, j, k and l movement commands and practice them in an adventure style interactive play. 

---
# ** vim command grammar**
## Motion operators `:help motion.txt`

### LINENO MOTIONOP

3g  "move cursor to the beginning of the 3rd line"  
30| "move cursor to column 30"

### [ COUNT ] MOTIONOP [ MODIFIER ]
3f; "move cursor to the 3rd semicolon right of the current position"  
2/RE &lt;CR&gt;  "find the second pattern matching the Regular Expression RE"

## File Operators
### [ LINENO|COUNT ] FILEOP

---
# ** vim command grammar**
## Edit operators without motion `:help insert.txt` `:help change.txt`

### [ REGISTER ]  [ COUNT ] EDITOP 
r = replace count char  
R = replace until ESC, repeat count times  
s = delete count chars and enter insert mode  
S = delete count lines and enter insert mode  
x = delete char under cursor  
X = delete char before cursor  
C = change the current line  
dd = delete the current line  
yy = yank the current line  
Y = shorthand for yy

---
# ** vim command grammar**
## Edit operators with motion `:help insert.txt` `:help change.txt`

### [ REGISTER ]  [ LINENO|COUNT ] EDITOP [ [ COUNT ] MOTIONOP ]  
d = delete  
D = shorthand for d$  
c = change  
y = yank  

`3dw` "delete three words"  
`c2t;` "change text up to second semicolon"  
`"a3d}` "Delete the next 3 paragraphs into register a"

---
# ** vim editing commands**
## Basic Motion operators `:help motion.txt`

Almost all motion operators allow for an optional count preceeding the operator.

   <table border=1>
<tr><th>char</th><th>operation with description; (s) means count prefix</th><th>arguments</th></tr>
<tr><td>h</td><td>char(s) left</td></tr>
<tr><td>j</td><td>line(s) down</td></tr>
<tr><td>k</td><td>line(s) up</td></tr>
<tr><td>l or space</td><td>char(s) right</td></tr>
<tr><td>f</td><td>find char(s) right</td><td>char</td></tr>
<tr><td>F</td><td>find char(s) left</td><td>char</td></tr>
<tr><td>t</td><td>up to char(s) right</td><td>char</td></tr>
<tr><td>T</td><td>up to char(s) left</td><td>char</td></tr>
<tr><td>0 (zero)</td><td>first char of line</td><td>no count</td></tr>
<tr><td>$</td><td>last char of line(s)</td><td>count - 1 lines down</td></tr>
<tr><td>|</td><td>goto column(s)</td><td>end of line if count is > than width</td></tr>
<tr><td>-</td><td>line(s) up, first nonblank char</td></tr>
<tr><td>+ or CR</td><td>line(s) down, first nonblank char</td></tr>
<tr><td>\_</td><td>line(s) down, first nonblank char</td><td>count - 1 lines down</td></tr>
<tr><td>G</td><td>goto line(s), default last</td></tr>
<tr><td>H</td><td>goto top line(s) of screen</td><td>count-1 down from top</td></tr>
<tr><td>L</td><td>goto bottom line(s) of screen</td><td>count-1 up from bottom</td></tr>
<tr><td>M</td><td>goto middle line of screen</td><td>count ignored</td></tr>
<tr><td>''</td><td>goto previous position(s), first nonblank char</td><td>count ignored</td></tr>
   </table>

---
# ** vim editing commands**
## More Motion operators `:help motion.txt`

Almost all motion operators allow for an optional count preceeding the operator.
   <table border=1>
<tr><th>char</th><th>operation with description; (s) means count prefix</th><th>arguments</th></tr>

<tr><td>Control D</td><td>scroll down lines(s)</td><td>count lines (sticky), default 1/2 screen</td></tr>
<tr><td>Control U</td><td>scroll up lines(s)</td><td>count lines (sticky), default 1/2 screen</td></tr>
<tr><td>Control E</td><td>scroll down lines(s)</td><td>count lines (sticky), default 1 line</td></tr>
<tr><td>Control Y</td><td>scroll up lines(s)</td><td>count lines (sticky), default 1 line</td></tr>
<tr><td>Control F</td><td>scroll down page(s)</td><td>count pages, default 1 page-1 line</td></tr>
<tr><td>Control B</td><td>scroll up page(s)</td><td>count pages, default 1 page-1 line</td></tr>
<tr><td>Control N</td><td>next line(s), same column</td><td></td></tr>
<tr><td>Control P</td><td>previous line(s), same column</td><td></td></tr>
<tr><td>%</td><td>goto matching "()[]{}" (s)</td><td>count cause undefined behaviour</td></tr>
<tr><td>/</td><td>scan for a RE pattern</td><td>pattern is sticky</td></tr>
<tr><td>?</td><td>scan backwards</td><td>pattern is sticky</td></tr>
<tr><td>n</td><td>scan for next instance(s) of pattern</td></tr>
<tr><td>N</td><td>scan for previous instance(s) of pattern</td></tr>
   </table>

---
# ** vim editing commands**
## Word oriented motion operators `:help motion.txt`

+ A **word** consists of a sequence of letters, digits and underscores, or a sequence of other non-blank characters, separated with white space (spaces, tabs, ). This can be changed with the 'iskeyword' option.

+ A **WORD** consists of a sequence of non-blank characters, separated with white space. An empty line is also considered to be a word and a WORD.

   <table border=1>
<tr><th>char</th><th>operation with description; (s) means count prefix</th></tr>

<tr><td>w</td><td>word(s) forward</td></tr>
<tr><td>b</td><td>word(s) backward</td></tr>
<tr><td>W</td><td>WORD(s) forward></tr>
<tr><td>B</td><td>WORD(s) backward</td></tr>
<tr><td>e</td><td>forward to the end of word(s)</td></tr>
<tr><td>E</td><td>forward to the end of WORD(s)</td></tr>
   </table>

---
# ** vim editing commands**
## Other motion operators `:help motion.txt`

+ A sentence is delimited by ".", "?" or "!"
+ A blank line defines a paragraph
+ The '{' and '}' in the first column are C language section definitions
+ The definition of paragraph and section nroff macros can be set using set command

   <table border=1>
<tr><th>char</th><th>operation with description; (s) means count prefix</th></tr>

<tr><td>(</td><td>sentence(s) backward</td></tr>
<tr><td>)</td><td>sentence(s) forward</td></tr>
<tr><td>{</td><td>paragraph(s) backward</td></tr>
<tr><td>}</td><td>paragraph(s) forward</td></tr>
<tr><td>]]</td><td>section(s) forward or to the next '{' in the first column.</td></tr>
<tr><td>][</td><td>section(s) forward or to the next '}' in the first column</td></tr>
<tr><td>[[</td><td>section(s) backward or to the previous '{' in the first column</td></tr>
<tr><td>[]</td><td>section(s) backward or to the previous '}' in the first column </td></tr>
   </table>

---
# ** vim editing commands**
##  Editing operators `:help change.txt`

   <table border=1>
<tr><th>char</th><th>operation with description</th></tr>

<tr><td>C</td><td>deletes text from the cursor to the end of the line and then puts the editor in insert mode. If a count is specified, it deletes an additional count-1 lines.</td></tr>
<tr><td>D</td><td>deletes to the end of the line. If preceded by a count, it deletes to the end of the line and count-1 more lines.</td></tr>
<tr><td>s</td><td>deletes a single character and puts the editor in insert mode. If preceded by a count, then count characters are deleted.</td></tr>
<tr><td>S</td><td>deletes the entire current line and puts the editor in insert mode. If a count is specified, it deletes count lines.</td></tr>
<tr><td>x</td><td>deletes the character under the cursor.  If preceded by a count, then count characters are deleted.</td></tr>
<tr><td>X</td><td>deletes the character left of the cursor.  If preceded by a count, then count characters are deleted.</td></tr>
   </table>

---
# ** vim editing commands **
## cut/paste/copy and registers `:help copy-move` `:help registers`

### When you delete something with the c, d, x, or another command, the text is saved in a register.  You can put it back by using the `p` command. 
### The `y` yank command copies the indicated text into a register.
### The other named registers are accessed with the double quote followed by register name.
If you yank or delete an entire line, it is put on the line after the cursor.  If you yank or delete part of a line, the p command puts it on the same line after the cursor.

#### There are ten types of registers
The unnamed register ""  
10 numbered registers "0 to "9  
The small delete register "-  
26 named registers "a to "z or "A to "Z   
Three read-only registers ":, "., "%  
Alternate buffer register "#  
The expression register "=  
The selection and drop registers "\*, "+ and "~  
The black hole register "\_  
Last search pattern register "/  

---
# ** vim visual mode commands** 

Visual mode is a flexible and easy way to select text for an operator.
It is the only way to select a block of text spanning multiple lines.
This just introduces the mode, the capabilities are comprehensive.

## Using Visual mode     `:help visual.txt`

1. Mark the start of the text with "v", "V" or CTRL-V.  
The character under the cursor will be used as the start.
2. Move to the end of the text.  
The text from the start of the Visual mode up to and including the
 character under the cursor is highlighted.
3. Type an operator command.  
The highlighted characters will be operated upon.

   <table border=1>
<tr><th>char</th><th>operation with description</th></tr>

<tr><td>v</td><td>start/stop visual mode per character</td></tr>
<tr><td>V</td><td>start/stop visual mode per line</td></tr>
<tr><td>CTRL-V</td><td>start/stop visual mode blockwise</td></tr>
<tr><td>ESC</td><td>cancel visual mode</td></tr>

   </table>

---
# ** vim advanced**

- .vimrc, .viminfo, .vim
- tags
- :make
- text-objects - inside commands
- tilde change case and :set tildeop for tilde motionOP
- increment/decrement CTRL-A CTRL-X
- indenting with =
- folds
- splits/windows
- tab-pages
- views
- sessions
- netrw
- insert-completion

---
# ** vim advanced**
## .vimrc startup commands `:help vimrc`

	!vim
	" Hex edit with xxd
	nmap <F7> :%!xxd<cr>
	nmap <F8> :%!xxd -r<cr>
	" what I like
	set paste
	set showmode
	syntax on
	set ignorecase
	set noai

### .viminfo remembers state for edited files `:help viminfo`

If you exit Vim and later start it again, you would normally lose a lot of
information.  The viminfo file can be used to remember that information, which
enables you to continue where you left off.

### .vim is the directory for autoload and plugins

---
# ** vim advanced**

## tags `:help tags`
### vim help uses tags

- exuberant ctags (I use universal ctags)
- index file with regular expressions
- tagname&lt;TAB&gt;filename&lt;TAB&gt;ex cmd;"&lt;TAB&gt;extension fields
- vi -t tag
- :tag tag  and :pop or Control t
- Control ] with cursor on tag
- `:stag tag` splits the window and positions the new one at the tag

---
# ** vim advanced**
## :make `:mak[e]! [arguments]`   `:help :make` `:help quickfix`

Vim knows how to run the unix make command and pass arguments.
If you have a Makefile in the current directory, then `:make` will run the make command.
Errors are captured in the file makeef and 

+ If the 'autowrite' option is on, write any changed buffers
+ An errorfile name is made from 'makeef'.
   If 'makeef' doesn't contain "##", and a file with this name already exists, it is deleted.
+ The program given with the 'makeprg' option is started (default "make")
   with the optional [arguments] and the output is saved in the errorfile
   (for Unix it is also echoed on the screen).
+ The errorfile is read using 'errorformat'.
+ If make! is given the first error is not jumped to.
+ The errorfile is deleted.
+ You can now move through the errors with commands like :cnext and :cprevious

---
# ** vim advanced**
## Text objects - inside commands `:help text-objects`

They are a bit more clever than normal motions because they can act in both “directions.”

The commands that start with "a" select "a"n object including white space,
the commands starting with "i" select an "inner" object without white space.

+ words (iw, iW, aw, aW)
+ sentences (is, as),
+ paragraphs (ip, ap),
+ blocks (a[, a], a<, a>, i[, i], i<, i>) 
+ parenthesis (a(, a), i(, i), ab, ib)
+ current function block (i{, i}, a{, a}, iB, aB)
+ HTML tags (at, it)
+ quotes (a", a', a`, i", i', i`)

---
# ** vim advanced**
## tilde change case `:help case`

When `:set notildeop`, `~` changes case at cursor and moves one to the right.

When `:set tildeop`, the command changes to `~ motionOP`.</br>
`~` takes the next keystroke(s) as motion.

For example, to change the case of an XML element with FOO in the middle:</br>
`<tagfoobar>this tag has foo in it</tagfoobar>`

    !vim
    :set tildeop
    /foo
    ~at

`<TAGFOOBAR>THIS TAG HAS FOO IN IT</TAGFOOBAR>`

To change the case of the text in the element:</br>
`<tagfoobar>this tag has foo in it</tagfoobar>`

    !vim
    :set tildeop
    /foo
    ~it

`<tagfoobar>THIS TAG HAS FOO IN IT</tagfoobar>`

---
# ** vim advanced**
## increment/decrement CTRL-A CTRL-X `:help CTRL-A` `:help CTRL-X`

[count]CTRL-A Add [count] to the number or alphabetic character at or after the cursor.

[count]CTRL-X Subtract [count] from the number or alphabetic character at or after the cursor.

In visual mode, the gCTRL-[AX] command will increment/decrement a series of highlighted lines.</br>
For Example, if you have this list of numbers:

        1.
        1.
        1.
        1.

Move to the second "1." and Visually select three lines.</br>
pressing g CTRL-A results in:

        1.
        2.
        3.
        4.

---
# ** vim advanced**
## indenting with =  `:help =` `:help C-indenting`

**`=MOTIONop`**
Filter MOTIONop lines through the program set with the 'equalprg' option.  When the 'equalprg' option is empty (default), use the internal formatting function C-indenting and 'lisp'.

**`[count]==`** Filter [count] lines like with =MOTIONop.

### format a C function

    !vim
    :set cindent
    /^{
    =%

---
# ** vim advanced**
## folds `:help folding` `:help usr_28.txt`

Folding is used to show a range of lines in the buffer as a single line on the
screen. The advantage of folding is that you can get a better overview of the
structure of text, by folding lines of a section and replacing it with a line
that indicates that there is a section.

**`zfMOTIONOp`**  Fold a set of lines

**`zo`**   Open a fold

**`zc`**   Close a fold

Folds can be nested. A region of text that contains folds can be folded
again.

**`zr`**   Reduce the folding in nested folds

**`zm`**   More folding in nested folds

**`zn`**   Disable folding

**`zN`**   More folding in nested folds

---
# ** vim advanced**
## splits/windows

---
# ** vim advanced**
## vimdiff/diffsplit `:help vimdiff` `:help diffsplit`

Vim will split the screen and show the result of the diff command in one window with the differences highlighted in the other.

From the command line:</br>
`vimdiff filever1 filever2`

---
# ** vim advanced**
## tab-pages `:help tab-pages`
You will have noticed that windows never overlap.
That means you quickly run out of screen space.
The solution for this is called Tab pages.

## `:tabedit file2` will open a new tab and take you to edit file2
## `:tabedit file3` will open a new tab and take you to edit file3  

### To navigate between these tabs, you can be in normal mode and type `gt` or `gT` to go to next tab or previous tab respectively. You can also navigate to a particular index tab (indexed from 1) using Ngt where N is the index of your tab. Example: `2gt` takes you to 2nd tab  
### To directly move to first tab or last tab, you can enter the following in command mode: `:tabfirst` or `:tablast` for first or last tab respectively. To move back and forth use `:tabn` for next tab and `:tabp` for previous tab.

## You can list all the open tabs using `:tabs`
## To open multiple files in tabs: $ `vim -p source.c source.h`
## To close a single tab: `:tabclose` or `:close` and to close all other tabs except the current one: `:tabonly` . Use the suffix! to override changes of unsaved files

---
# ** vim advanced**
## views `:help views` `:help usr_21.txt`

A View is a collection of settings that apply to one window.  You can save a
View and when you restore it later, the text is displayed in the same way.
The options and mappings in this window will also be restored, so that you can
continue editing like when the View was saved.

**`:mkvie[w][!] [file]`** Write a Vim view script.</br>
When [!] is included an existing file is overwritten.</br>
When [file] is omitted or is a number from 1 to 9, a name is generated and 'viewdir' prepended.</br>
When the last path part of 'viewdir' does not exist, this directory is created.</br>
E.g., when 'viewdir' is "$VIM/vimfiles/view" then "view" is created in "$VIM/vimfiles".

**`:lo[adview] [nr]`** Load the view for the current file.</br>
When [nr] is omitted, the view stored with ":mkview" is loaded.
When [nr] is specified, the view stored with ":mkview [nr]" is loaded.

You might want to clean up your 'viewdir' directory now and then.

---
# ** vim advanced**
## sessions `:help session` `:help 21.4`

A Session keeps the Views for all windows, plus the global settings.  You can
save a Session and when you restore it later the window layout looks the same.
You can use a Session to quickly switch between different projects, 
automatically loading the files you were last working on in that project.

Views and Sessions are a nice addition to viminfo-files, which are used to
remember information for all Views and Sessions together.

`:help viminfo-file`

The following command creates a session file:
`:mksession mySession.vim`

You can quickly start editing with a previously saved View or Session with the
-S argument:    `vim -S mySession.vim`

---
# ** vim advanced**
## netrw browsing `:help browsing`

Netrw supports the browsing of directories on your local system and on remote hosts.

### Browsing includes:

+ listing files and directories
+ entering directories
+ editing files therein
+ deleting files/directories
+ making new directories
+ moving (renaming) files and directories
+ copying files and directories
+ etc.

### To browse a directory, simply "edit" it!

---
# ** vim advanced**
## netrw remote files `:help pi_netrw` `:help netrw-intro-browsing`

Netrw makes reading, writing and browsing over a network protocol easy.</br>
Make sure plugins are enabled.

    !vim
    :set nocp
    :filetype plugin on

### Supported protocols include:

    !vim
    dav
    fetch
    ftp
    http
    rcp
    rsync
    scp
    sftp

Generally, one may just use the URL notation with a normal editing command

    `:e ftp://[user@]machine/path`

---
# ** vim advanced**
## insert-completion

---
# ** vim plugins **

Plugins really need a plugin manager, there are several.  I use Vundle.
Useful plugins depend on your workflow.  Autosave is pretty helpful.

## Vundle can use github to fetch and install plugins.

`git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`

## Add this to .vimrc

    !vim
    " vundle setup
    set nocompatible              " be iMproved, required
    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()

    " let Vundle manage Vundle, required
    Plugin 'VundleVim/Vundle.vim'
    Plugin '907th/vim-auto-save'

    call vundle#end()            " required
    filetype plugin indent on    " required

---
# **examples of what I use**

    }                 # move cursor to next paragraph
    ]]                # move cursor to next function
    ''                # jump to the previous edit location
    dt<               # delete from cursor up to '<'

    !Gsort<CR>        # sort from current to last line
    !}cat -n<CR>      # number the paragraph lines
    !!ls<CR>          # replace line with output of ls
    !!date<CR>        # replace line with output of date
    !!!<CR>           # replace the line with output of prev cmd
    
    =%                # format to the matching bracket
    n.                # repeat the previous search and edit
    :map K .n<CR>     # set the K key to run the n. commands
    ;.                # find previous find char and repeat command

    # global column swap for : delimited 2 column text
    :%/s/\(.*\):\(.*\)/\2:\1

    :ta tag<CR>       # jump to tag
    Control ]         # jump to tag under cursor
    :e!%<CR>          # discard and re-edit. Autosave negates this.

---
#** Sam Chessman **

## Slides for this presentation available at schessman.gitlab.io:

+ **HTML** https://sscpresentations.gitlab.io/42\_years\_of\_vi.html
+ **PDF** https://sscpresentations.gitlab.io/42\_years\_of\_vi.pdf

## Source for this presentation available at:
+ **WEB** https://gitlab.com/sscpresentations/sscpresentations.gitlab.io
+ **SRC** https://gitlab.com/sscpresentations/42-years-of-vi

## I may answer mail at sam.chessman@gmail.com
