# Makefile to create 42_years_of_vi.html
NAM=42_years_of_vi
SRC=$(NAM).md
DOT=$(NAM).dot
HTML=$(SRC:.md=.html)
PDF=$(SRC:.md=.pdf)
PNG=$(DOT:.dot=.png)
CSS=/dev/null
#LOPTS=-x extra
DOPTS=-Tpng -Nfontsize=48 -Efontsize=48 -Efontname=Helvitica-Bold
DISTDIR=../sscpresentations.gitlab.io/public

all: $(HTML) images/$(PNG)

# wkhtmltopdf needs css, js and images directories, run in distdir
dist: all
	cp -v $(HTML) $(DISTDIR)
	cp -v images/$(PNG) $(DISTDIR)/images
	cd $(DISTDIR);  wkhtmltopdf -s A5 -O landscape $(HTML) $(NAM).pdf

$(HTML): $(SRC)
	landslide $(SRC) $(LOPTS) -d $(HTML)
	sed -i \
	 -e '/file:/s;file:.*/images;images;' \
	 -e '/file:.*css/s;file:.*css/;css/;' \
	 -e '/file:.*js/s;file:.*js/;js/;' \
     $(HTML)

images/$(PNG): $(DOT)
	dot $(DOT) $(DOPTS) | convert - -resize 20% $@
